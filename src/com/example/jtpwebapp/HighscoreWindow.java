package com.example.jtpwebapp;

import java.util.List;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class HighscoreWindow extends Window {

	TreeTable table;
	MySQL sql;

	public HighscoreWindow(String string, MySQL mysql) {
		super(string);
		setModal(true);
		this.sql = mysql;

		VerticalLayout layout = new VerticalLayout();

		NativeSelect select = new NativeSelect();

		for (int i = 1; i <= 5; i++) {
			select.addItem(i);
			select.setItemCaption(i, "" + i);
		}

		select.setNullSelectionAllowed(false);
		select.setValue(3);
		select.setImmediate(true);
		select.setVisible(true);

		select.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(final ValueChangeEvent event) {
				final int valueString = (Integer) event.getProperty()
						.getValue();
				populateTable(valueString);
			}
		});

		Label difficultyLabel = new Label("Difficulty: ");
		HorizontalLayout diffLayout = new HorizontalLayout();
		diffLayout.addComponent(difficultyLabel);
		Panel p = new Panel();
		p.setWidth("10");
		diffLayout.addComponent(p);
		diffLayout.addComponent(select);

		layout.addComponent(diffLayout);

		table = new TreeTable();
		table.addContainerProperty("Name", String.class, "");
		table.addContainerProperty("Time", Double.class, 0);

		populateTable(3);

		layout.addComponent(table);
		setContent(layout);
	}

	public void populateTable(int difficulty) {
		table.removeAllItems();

		List<Score> list = sql.select(difficulty);
		for (Score s : list) {

			final Object projectId = table.addItem(new Object[] {
					s.getName(), s.getTime() }, null);
			table.setChildrenAllowed(projectId, false);
		}
		
		table.sort();
	}

}
