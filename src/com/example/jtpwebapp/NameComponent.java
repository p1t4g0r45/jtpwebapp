package com.example.jtpwebapp;

import com.vaadin.annotations.Theme;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

@Theme("jtpwebapp")
public class NameComponent extends CustomComponent implements Property.ValueChangeListener{

	private static final long serialVersionUID = 8878299908406387263L;
	private TextField textfield;
	private Label text;
	private String name;
	private Button edit;
	MyComponent mc;
	
	public NameComponent(final MyComponent mc){
		
		this.mc = mc;
		
		Panel panel = new Panel();
		Layout layout = new VerticalLayout();
		textfield = new TextField();
		textfield.setImmediate(true);
		textfield.addValueChangeListener(this);
		text= new Label("Enter name:");
		edit = new Button("Edit");
		edit.setVisible(false);
		edit.setDisableOnClick(true);
		edit.addClickListener(new Button.ClickListener() {
			
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				textfield.setEnabled(true);
				
			}
		});
		
		
		layout.addStyleName("layoutPlayerName");
		panel.addStyleName("namePanel");
		textfield.addStyleName("playerNameInputField");
		text.addStyleName("playingAs");
		
		Layout layout2 = new HorizontalLayout();
		layout2.setSizeUndefined();
		layout2.setHeight(""+25);
		layout2.addComponent(text);
		layout2.addComponent(edit);
		
		layout.addComponent(layout2);
		layout.addComponent(textfield);
		layout.setHeight(""+50);
		
		panel.setSizeUndefined();

		panel.setContent(layout);
		
		
		setCompositionRoot(panel);
		
	}
	
	@Override
	public void valueChange(ValueChangeEvent event) {
		this.name = (String)event.getProperty().getValue();
		mc.getState().name = name;
		if(name.equals("")){
			
			// "^(?=\\s*\\S).*$"
			
			this.text.setValue("Enter name:");
			this.textfield.setEnabled(true);
			edit.setVisible(true);
			edit.setEnabled(false);
			return;
		}
		this.text.setValue("You are playing as "+name);
		this.textfield.setEnabled(false);
		edit.setVisible(true);
		edit.setEnabled(true);
		
	}
	
	public String getName(){
		return name;
	}

}
