package com.example.jtpwebapp;

import com.example.jtpwebapp.client.mycomponent.MyComponentClientRpc;
import com.example.jtpwebapp.client.mycomponent.MyComponentServerRpc;
import com.example.jtpwebapp.client.mycomponent.MyComponentState;
import com.vaadin.server.Page;
import com.vaadin.shared.MouseEventDetails;
import com.vaadin.shared.Position;
import com.vaadin.ui.Notification;

public class MyComponent extends com.vaadin.ui.AbstractComponent {

	private MyComponentServerRpc rpc = new MyComponentServerRpc() {


		@Override
		public void clicked(MouseEventDetails mouseDetails) {
			// TODO Auto-generated method stub

			getRpcProxy(MyComponentClientRpc.class).alert("Well Done!");
		}

		@Override
		public void message(String msg) {
			// TODO Auto-generated method stub
			// getRpcProxy(MyComponentClientRpc.class).alert(msg);
			Notification sample = new Notification(getState().name, msg, Notification.Type.ERROR_MESSAGE);
			sample.setDelayMsec(10000);
			sample.setPosition(Position.BOTTOM_CENTER);
			sample.show(Page.getCurrent());

		}

		/*
		 * @Override public void scored(Score score) {
		 * 
		 * String msg = "your time: " + score.getTime() + "s \n" +
		 * "(difficulty " + score.getDifficulty() + ")"; Notification sample =
		 * new Notification(getState().name, msg); sample.setDelayMsec(10000);
		 * sample.show(Page.getCurrent());
		 * 
		 * 
		 * MySQL.insertScore(score);
		 * 
		 * }
		 */

		@Override
		public void scored(int difficulty, double time) {
			String msg = "Your time: " + time + "s \n" + "(difficulty "
					+ difficulty + ")";
			Notification sample = new Notification(msg, Notification.Type.WARNING_MESSAGE);
			sample.setDelayMsec(10000);
			sample.setPosition(Position.BOTTOM_CENTER);
			sample.show(Page.getCurrent());

			Score score = new Score();
			score.setDifficulty(difficulty);
			score.setTime(time);
			score.setName(getState().name);
			MySQL.insertScore(score);
		}

		/*
		 * @Override public void circles(List<MyCircle> list) { // TODO
		 * Auto-generated method stub for( MyCircle c : list){
		 * c.setX(c.getX()+1); c.setY(c.getY()+1); }
		 * 
		 * }
		 */

	};

	public MyComponent() {
		registerRpc(rpc);
	}

	@Override
	public MyComponentState getState() {
		return (MyComponentState) super.getState();
	}
}
