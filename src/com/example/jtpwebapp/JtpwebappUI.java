package com.example.jtpwebapp;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Widgetset;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Audio;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Slider;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
@Theme("jtpwebapp")
@Widgetset("com.example.jtpwebapp.JtpwebappWidgetset")
public class JtpwebappUI extends UI {

	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = JtpwebappUI.class, widgetset = "com.example.jtpwebapp.JtpwebappWidgetset")
	public static class Servlet extends VaadinServlet {
	}

	private double difficulty;
	MyComponent mc;

	final String[] files = { "Ancora", "Andare", "Berlin_Song", "Dietro_Casa",
			"Divenire", "Eros", "Fairytale", "Fly", "High_Heels", "I_Giorni",
			"Indaco", "L'origine_Nascosta", "Lady_Labyrinth", "Le_Onde",
			"Love_In_A_Mystery", "Melodia_Africana", "Monday", "Nefeli",
			"Nightbook", "Nuvole_Bianche", "Oltremare", "Passaggio",
			"Primavera", "Questa_Notte", "Solo", "The_Earth_Prelude",
			"White_Night", };

	@Override
	protected void init(VaadinRequest request) {

		
		mc = new MyComponent();
		

		Button highscore = new Button("Highscore");

		final VerticalLayout layout = new VerticalLayout();
		layout.setMargin(true);
		setContent(layout);
		NameComponent nc = new NameComponent(mc);
		layout.addComponent(nc);

		VerticalLayout vertical = new VerticalLayout();
		HorizontalLayout horizontal = new HorizontalLayout();

		Label level = new Label("</br>Select difficulty level",
				ContentMode.HTML);
		level.setSizeUndefined();
		vertical.addComponent(level);

		final Slider slider = new Slider();
		slider.setMax(5);
		slider.setMin(1);
		slider.setWidth(300, Unit.PIXELS);
		slider.setValue(3.);
		slider.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				difficulty = slider.getValue();
				mc.getState().radius = (int) difficulty;
			}
		});

		vertical.addComponent(slider);

		horizontal.addComponent(vertical);
		Panel p = new Panel();
		p.setWidth("100");
		horizontal.addComponent(p);

		VerticalLayout vertical2 = new VerticalLayout();
		Panel p2 = new Panel();
		p2.setHeight("20");
		vertical2.addComponent(p2);
		vertical2.addComponent(highscore);

		horizontal.addComponent(vertical2);
		layout.addComponent(horizontal);

		// MyEntryPoint mep = new MyEntryPoint();
		// DrawingArea ds = new DrawingArea(400, 400);
		// final MyWidget mywidget = new MyWidget();
		// RootPanel.get().add(mywidget);
		layout.addComponent(mc);

		final Audio audio = new Audio();

		final String url = "http://baranski-family.pl/sites/default/files/einaudi/";
		HorizontalLayout audioLayout = new HorizontalLayout();
		audioLayout.addComponent(audio);
		
		ExternalResource resource = new ExternalResource(url
				+ files[0] + ".mp3");
		audio.setSource(resource);

		NativeSelect select = new NativeSelect("Select soundtrack");


		int i = 0;
		for (String name : files) {
			select.addItem(i);
			select.setItemCaption(i, name.replace("_", " "));
			i++;
		}

		select.setNullSelectionAllowed(false);
		select.setValue(0);
		select.setImmediate(true);
		select.setVisible(true);

		select.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(final ValueChangeEvent event) {
				final int valueString = (Integer) event.getProperty()
						.getValue();
				ExternalResource resource = new ExternalResource(url
						+ files[valueString] + ".mp3");
				audio.setSource(resource);
				audio.play();
			}
		});
		audioLayout.addComponent(select);

		layout.addComponent(audioLayout);

		Notification not = new Notification(
				"Click the circles as fast as possible!");
		not.setDelayMsec(2000);
		not.show(Page.getCurrent());

		final MySQL sql = new MySQL();
		// createAndRead();
		// createAndRollback();
		// sql.select(3);
		highscore.addClickListener(new ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				final Window window = new HighscoreWindow("Highscore", sql);
				UI.getCurrent().addWindow(window);
			}
		});

	}

	@SuppressWarnings("unused")
	private void createAndRead() {
		Score s = new Score();
		s.setId(1L);
		s.setName("name14");
		s.setDifficulty(30);
		s.setTime(16.456);
		MySQL.em.getTransaction().begin();
		MySQL.em.persist(s);
		MySQL.em.getTransaction().commit();

		// g should be written to database now.
		// Read it from db (no transaction context needed for em.find method)
		// Score g2 =
		MySQL.em.find(Score.class, s.getId());
	}
	/*
	 * private void createAndRollback() { Greeting g = new Greeting();
	 * g.setId(2L); g.setMessage("hello, createAndRollback");
	 * em.getTransaction().begin(); em.persist(g);
	 * em.getTransaction().rollback();
	 * 
	 * logger.info("Persisted " + g + ", but the transaction was rolled back.");
	 * Greeting g2 = em.find(Greeting.class, g.getId()); logger.info("Greeting "
	 * + g.getId() + " from db: " + g2); // should be // null }
	 */

}