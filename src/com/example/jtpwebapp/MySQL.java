package com.example.jtpwebapp;

import static org.eclipse.persistence.config.PersistenceUnitProperties.JDBC_DRIVER;
import static org.eclipse.persistence.config.PersistenceUnitProperties.JDBC_PASSWORD;
import static org.eclipse.persistence.config.PersistenceUnitProperties.JDBC_URL;
import static org.eclipse.persistence.config.PersistenceUnitProperties.JDBC_USER;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class MySQL {

	private static final String PERSISTENCE_UNIT_NAME = "greetingPU";
	private EntityManagerFactory emf;
	public static EntityManager em;

	
	  private String dbName = "jbarans1"; private String dbUserName =
	  "jbarans1"; private String dbPassword = "jK9WvMZg"; private String host =
	  "jdbc:mysql://mysql.agh.edu.pl:3306/" + dbName;
	 

	// localhost:12345 mysql.agh.edu.pl:3306
	// tunnel port:12345, destination: mysql.agh.edu.pl:3306, local
/*
	private String dbName = "jtpwebapp";
	private String dbUserName = "adminNJy6FAH";
	private String dbPassword = "QeL7i3XPJPGh";
	private String host = "jdbc:mysql://127.13.81.2:3306/" + dbName; */

	public MySQL() {
		Map<String, String> properties = new HashMap<String, String>();

		// Configure the internal EclipseLink connection pool

		properties.put(JDBC_DRIVER, "com.mysql.jdbc.Driver");
		properties.put(JDBC_URL, host);
		properties.put(JDBC_USER, dbUserName);
		properties.put(JDBC_PASSWORD, dbPassword);

		emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME,
				properties);
		em = emf.createEntityManager();

	}

	public static void insertScore(Score score) {
		int difficulty = score.getDifficulty();
		String name = score.getName();
		double time = score.getTime();

		if (time < 4)
			return;

		int count = 0;
		double maxTime = 0;
		Long maxId = null;

		Query q = em.createQuery("select s from Score s");
		List<Score> scores = q.getResultList();
		for (Score s : scores) {
			int x = 0;
			if (s.getDifficulty() == difficulty) {
				if (s.getTime() > maxTime) {
					maxTime = s.getTime();
					maxId = s.getId();
				}
				if (s.getName().equals(name)) {
					if (s.getTime() > time) {
						// lepszy wynik, aktualizacja

						// System.out.println("lepszy, aktualizacja");

						em.getTransaction().begin();
						Score scoreUpdate = (Score) em.find(Score.class,
								s.getId());
						scoreUpdate.setTime(time);
						em.merge(scoreUpdate);
						em.getTransaction().commit();
						return;
					} else {
						// System.out.println("nie lepszy, nic");
						return;
					}
				}
				count++;
			}
		}

		if (count >= 10) {
			// 10 wynikow, sprawdzam czy do wsadzenia jest lepszy od najgorszego
			if (maxTime > time) {
				// usuwamy element z minTimem i wsadzamy nowy
				Score scoreMax = (Score) em.find(Score.class, maxId);
				em.getTransaction().begin();
				em.remove(scoreMax);
				em.persist(score);
				em.getTransaction().commit();
				// System.out.println("10, jestesmy lepsi " + maxId);
				return;
			}
			// System.out.println("10, nie jestesmy lepsi");
			return;
		}
		// System.out.println("nie ma 10 w tym diff");

		em.getTransaction().begin();
		em.persist(score);
		em.getTransaction().commit();
	}

	public static List<Score> select(int difficulty) {

		List<Score> list = new ArrayList<Score>();

		Query q = em.createQuery("select s from Score s");
		List<Score> scores = q.getResultList();
		for (Score s : scores) {
			if (s.getDifficulty() == difficulty) {
				list.add(s);
			}
		}
		Collections.sort(list);
		return list;

	}

}
