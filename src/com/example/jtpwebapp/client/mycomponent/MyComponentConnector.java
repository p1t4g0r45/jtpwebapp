package com.example.jtpwebapp.client.mycomponent;

import java.util.ArrayList;
import java.util.List;

import org.vaadin.gwtgraphics.client.animation.Animate;
import org.vaadin.gwtgraphics.client.shape.Circle;

import com.example.jtpwebapp.MyComponent;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.client.communication.RpcProxy;
import com.vaadin.client.communication.StateChangeEvent;
import com.vaadin.client.ui.AbstractComponentConnector;
import com.vaadin.shared.ui.Connect;

@Connect(MyComponent.class)
public class MyComponentConnector extends AbstractComponentConnector {

	MyComponentServerRpc rpc = RpcProxy
			.create(MyComponentServerRpc.class, this);
	int clicked = 0;
	int radius = 50;
	long start;
	private int difficulty;
	List<Animate> animations = new ArrayList<Animate>();

	public MyComponentConnector() {

		// List<MyCircle> list = getWidget().getCircles();

		registerRpc(MyComponentClientRpc.class, new MyComponentClientRpc() {
			

			public void alert(String message) {
				// TODO Do something useful
				Window.alert(message);
			}
		});

		// TODO ServerRpc usage example, do something useful instead
		List<Circle> list = getWidget().getCircles();
		for (final Circle c : list) {
			c.addClickHandler(new ClickHandler() {
				public void onClick(ClickEvent event) {
					/*final MouseEventDetails mouseDetails = MouseEventDetailsBuilder
							.buildMouseEventDetails(event.getNativeEvent(),
									getWidget().getElement());*/
					/*
					 * if (!getState().circleAdded) { final Circle c = new
					 * Circle(event.getX(), event.getY(), 5);
					 * getWidget().getDrawingArea().add(c);
					 * getState().circleAdded = true; }
					 */

					if (getState().name.equals("")) {
						rpc.message("Enter name, and choose a difficulty level!");
						return;
					}

					if (clicked == 0)
						start = System.currentTimeMillis();

					if (c.getRadius() == radius) {
						Animate a = new Animate(c, "radius", c.getRadius(), 0,
								1000);
						a.start();
						animations.add(a);
						clicked++;
						getState().circleClicked = true;
						if (clicked == 20) {
							long end = System.currentTimeMillis() - start;
							double time = end/1000 + (double)(end%1000)/1000;
							/*
							 * rpc.message(getState().name+", your time: " + end
							 * / 1000 + "," + end % 1000 +
							 * "s \n"+"(difficulty "+difficulty+")");
							 */
							/*rpc.message("your time: " + end / 1000 + "," + end
									% 1000 + "s \n" + "(difficulty "
									+ difficulty + ")");*/
							/*
							Score score = new Score();
							score.setDifficulty(difficulty);
							score.setTime((end-start)/1000);

							score.setName(getState().name);
							rpc.scored(score );*/

							rpc.scored(difficulty, time);
							// reload //

							reload();

						}
					}
				}

			});
		}

	}

	protected void reload() {
		for (Animate a : animations) {
			a.stop();
		}
		for (Circle c : getWidget().getCircles()) {
			c.setX(getWidget().randomX());
			c.setY(getWidget().randomY());
			c.setRadius(radius);
			c.setFillColor(getWidget().randomColor());
		}
		clicked = 0;
		getState().circleClicked = false;

	}

	@Override
	protected Widget createWidget() {
		return GWT.create(MyComponentWidget.class);
	}

	@Override
	public MyComponentWidget getWidget() {
		return (MyComponentWidget) super.getWidget();
	}

	@Override
	public MyComponentState getState() {
		return (MyComponentState) super.getState();
	}

	@Override
	public void onStateChanged(StateChangeEvent stateChangeEvent) {
		super.onStateChanged(stateChangeEvent);

		if (clicked == 0) {

			radius = 6*(6-getState().radius);
			difficulty = getState().radius;
			// TODO do something useful
			for (Circle c : getWidget().getCircles()) {
				c.setRadius(radius);
			}
		}
	}

}
