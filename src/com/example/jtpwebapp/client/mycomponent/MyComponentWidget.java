package com.example.jtpwebapp.client.mycomponent;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.vaadin.gwtgraphics.client.DrawingArea;
import org.vaadin.gwtgraphics.client.shape.Circle;

import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

// TODO extend any GWT Widget
public class MyComponentWidget extends HorizontalPanel {

	private final static int WIDTH = 1270, HEIGHT = 420, RADIUS = 10;
//private final static int WIDTH = (int) (Resolution.width*0.9), HEIGHT = Resolution.height-320, RADIUS = 10;
	private final DrawingArea area;
	final List<Circle> circles = new ArrayList<Circle>();

	public int clickedCircles = 0;
	Random rand = new Random();

	public MyComponentWidget() {
		// setText("MyComponent sets the text via MyComponentConnector using MyComponentState");
		setStyleName(CLASSNAME);

		setVisible(true);

		VerticalPanel vPanel = new VerticalPanel();
		add(vPanel);

		area = new DrawingArea(WIDTH, HEIGHT);
		vPanel.add(area);
		area.setStyleName("drawing-area");
		for (int i = 0; i < 20; i++) {

			Circle c = new Circle(randomX(), randomY(), RADIUS);
			c.setFillOpacity(0.5);
			c.setFillColor(randomColor());
			c.setVisible(true);
			circles.add(c);
			area.add(c);

		}

	}

	public static final String CLASSNAME = "mycomponent";

	public DrawingArea getDrawingArea() {
		return area;
	}

	public List<Circle> getCircles() {
		return circles;
	}

	public int randomX() {
		return rand.nextInt(WIDTH - 7 * RADIUS) + 35;
	}

	public int randomY() {
		return rand.nextInt(HEIGHT - 7 * RADIUS) + 35;
	}

	public String randomColor() {
		return "rgb(" + rand.nextInt(255) + ", " + rand.nextInt(255) + ", "
				+ rand.nextInt(255) + ")";
	}
}